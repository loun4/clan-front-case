import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { combineReducers } from 'redux-immutable';
import { Map, Iterable } from 'immutable';
import session from './reducers/session';
import entities from './reducers/entities';

const loggerMiddleware = createLogger({
  predicate() {
    return process.env.NODE_ENV === 'development';
  },

  stateTransformer: state => {
    if (Iterable.isIterable(state)) return state.toJS();
    return state;
  },
});

const rootReducer = combineReducers({ session, entities });

const store = createStore(
  rootReducer,
  Map({}),
  applyMiddleware(thunkMiddleware, loggerMiddleware)
);

export default store;
