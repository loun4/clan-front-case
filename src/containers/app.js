import 'bulma/css/bulma.css';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import Main from './main';
import SignIn from '../components/signin';
import Loader from '../components/loader';
import { authenticate } from '../actions/session';
import { mergeProps } from '../helpers';

import '../css/app.css';

class App extends Component {
  static propTypes = {
    session: ImmutablePropTypes.map.isRequired,
    authenticate: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const credentials = this.props.session.get('credentials');
    if (credentials) {
      this.props.authenticate(credentials);
    }
  }

  render() {
    const { session } = this.props;
    const { isFetching: isSessionFetching, isAuthenticated } = session.toJS();
    const willAuthenticate = session.get('credentials') !== null;

    if (willAuthenticate || isSessionFetching) {
      return <Loader />;
    }

    return (
      <div className="hero is-fullheight">
        {isAuthenticated ? (
          <Main />
        ) : (
          <SignIn authenticate={this.props.authenticate} />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.get('session'),
});

export default connect(
  mapStateToProps,
  { authenticate },
  mergeProps
)(App);
