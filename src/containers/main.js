import React from 'react';
import { connect } from 'react-redux';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import Header from '../components/header';
import Benefits from './benefits';
import { deauthenticate } from '../actions/session';
import { mergeProps } from '../helpers';

// eslint-disable-next-line no-shadow
const Main = ({ user, deauthenticate }) => (
  <React.Fragment>
    <Header user={user} deauthenticate={deauthenticate} />
    <Benefits />
  </React.Fragment>
);

Main.propTypes = {
  user: ImmutablePropTypes.map.isRequired,
  deauthenticate: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  user: state.getIn(['session', 'user']),
});

export default connect(
  mapStateToProps,
  { deauthenticate },
  mergeProps
)(Main);
