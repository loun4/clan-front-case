import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import numeral from 'numeral';
import { Map } from 'immutable';
import PeriodProgress from '../components/period-progress';
import { getEntityData } from '../actions/entities';
import Loader from '../components/loader';
import { mergeProps } from '../helpers';

const formatEuro = value => numeral(value).format('0,000 $');

export class Benefit extends Component {
  static propTypes = {
    getEntityData: PropTypes.func.isRequired,
    benefit: ImmutablePropTypes.map,
    history: PropTypes.shape().isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        slug: PropTypes.string,
      }),
    }).isRequired,
  };

  static defaultProps = {
    benefit: Map({}),
  };

  state = {
    isFetching: false,
  };

  constructor(props) {
    super(props);
    document.addEventListener('keyup', this.handleClose);
  }

  componentDidMount() {
    this.mounted = true;
    this.setState({ isFetching: true }, () =>
      this.props
        .getEntityData({
          entity: 'benefits',
          endpoint: `employee/damien/${this.props.match.params.slug}`,
          globalFetching: false,
        })
        .finally(() => {
          if (this.mounted) {
            this.setState({ isFetching: false });
          }
        })
    );
  }

  componentWillUnmount() {
    this.mounted = false;
    document.removeEventListener('keyup', this.handleClose);
  }

  handleClose = e => {
    if (!e.key || e.key === 'Escape') {
      this.props.history.push(`/`);
    }
  };

  renderQuota = ({
    id,
    totalPrice,
    teamContribution,
    consumedAmount,
    remainingAmount,
  }) => (
    <ul key={`quota-${id}`}>
      <li>
        <strong>total : </strong>
        {formatEuro(totalPrice)}
      </li>
      <li>
        <strong>contribution : </strong>
        {formatEuro(teamContribution)}
      </li>
      <li>
        <strong>montant utilisé : </strong>
        {formatEuro(consumedAmount)}
      </li>
      <li>
        <strong>montant restant : </strong>
        {formatEuro(remainingAmount)}
      </li>
    </ul>
  );

  renderLoader = () => (
    <div className="modal is-active">
      <div className="modal-background" />
      <Loader className="modal-card" />
    </div>
  );

  render() {
    const { benefit } = this.props;
    const { isFetching } = this.state;

    const { name, description, currentPeriod } = benefit.toJS();
    const {
      openingDateFormatted,
      closingDateFormatted,
      remainingDaysBeforeEnd,
      totalAllowedAmount,
      remainingAmount,
      numberOfParticipants,
      sollicitatedAmount,
      notExhaustedQuota = [],
    } = currentPeriod;

    if (isFetching) {
      return this.renderLoader();
    }

    return (
      <div className="modal is-active">
        <div className="modal-background" />
        <div className="modal-card">
          <header className="modal-card-head">
            <h1 className="modal-card-title">{name}</h1>
            <button
              type="button"
              className="delete"
              onClick={this.handleClose}
            />
          </header>
          <section className="modal-card-body">
            <div className="content">
              <PeriodProgress
                openingDateFormatted={openingDateFormatted}
                closingDateFormatted={closingDateFormatted}
                remainingDaysBeforeEnd={remainingDaysBeforeEnd}
              />
              <h2>Description</h2>
              <p>{description}</p>

              <h2>Nombre de participants</h2>
              <p>{numberOfParticipants}</p>

              <h2>Montant</h2>
              <ul>
                <li>
                  <strong>maximum : </strong>
                  {formatEuro(totalAllowedAmount)}
                </li>
                <li>
                  <strong>restant : </strong>
                  {formatEuro(remainingAmount)}
                </li>
                <li>
                  <strong>sollicité : </strong>
                  {formatEuro(sollicitatedAmount)}
                </li>
              </ul>

              <h2>Quotas</h2>
              <React.Fragment>
                {notExhaustedQuota.map(this.renderQuota)}
              </React.Fragment>
            </div>
          </section>
          <footer className="modal-card-foot">
            <button
              id="close"
              type="button"
              className="button"
              onClick={this.handleClose}
            >
              Retour
            </button>
          </footer>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  benefit: state
    .getIn(['entities', 'benefits', 'models'])
    .find(benefit => benefit.get('slug') === props.match.params.slug),
});

export default connect(
  mapStateToProps,
  { getEntityData },
  mergeProps
)(Benefit);
