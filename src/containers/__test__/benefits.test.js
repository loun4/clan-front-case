import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import mockState from './mock-state';
import Benefits from '../benefits';

const mockGetEntityData = jest.fn();
const mockStore = configureStore();

test('Render benefits', () => {
  const tree = renderer
    .create(
      <Router>
        <Benefits
          store={mockStore(mockState)}
          getEntityData={mockGetEntityData}
        />
      </Router>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test('Call getEntityData on componentDidMount with correct entity & endpoint', () => {
  mount(
    <Router>
      <Benefits
        store={mockStore(mockState)}
        getEntityData={mockGetEntityData}
      />
    </Router>
  );

  expect(mockGetEntityData.mock.calls[0][0]).toEqual({
    entity: 'benefits',
    endpoint: 'employee/damien/benefits',
  });
});
