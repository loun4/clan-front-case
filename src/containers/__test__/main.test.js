import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import renderer from 'react-test-renderer';
import mockState from './mock-state';
import Main from '../main';

const mockStore = configureStore([thunk]);

test('Render main', () => {
  const tree = renderer
    .create(
      <Provider store={mockStore(mockState)}>
        <Router>
          <Main deauthenticate={jest.fn()} />
        </Router>
      </Provider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
