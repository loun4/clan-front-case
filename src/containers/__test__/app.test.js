import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import renderer from 'react-test-renderer';
import { Map } from 'immutable';
import App from '../app';

const initialState = Map({
  session: Map({
    credentials: null,
  }),
});

const mockAuthenticate = jest.fn();
const mockStore = configureStore();

const makeWrapper = (state = initialState, mockFn = mockAuthenticate) => {
  return shallow(<App store={mockStore(state)} authenticate={mockFn} />)
    .shallow()
    .shallow();
};

test('Render app', () => {
  const tree = renderer
    .create(
      <App store={mockStore(initialState)} authenticate={mockAuthenticate} />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test('Render <Loader /> if persisted credentials', () => {
  const wrapper = makeWrapper(
    initialState.setIn(['session', 'credentials'], {
      email: 'test@test.com',
      password: '123456',
    })
  );

  expect(wrapper.find('Loader')).toHaveLength(1);
});

test('Call authenticate on componentDidMount with persisted credentials as arguments', () => {
  // eslint-disable-next-line no-unused-vars
  const wrapper = makeWrapper(
    initialState.setIn(['session', 'credentials'], {
      email: 'test@test.com',
      password: '123456',
    })
  );

  expect(mockAuthenticate.mock.calls[0][0]).toEqual({
    email: 'test@test.com',
    password: '123456',
  });
});

test('Render <Connect(Main) /> if authenticated', () => {
  const wrapper = makeWrapper(
    initialState.setIn(['session', 'isAuthenticated'], true)
  );

  expect(wrapper.find('Connect(Main)')).toHaveLength(1);
});

test('Render <SignIn /> if deauthenticated', () => {
  const wrapper = makeWrapper();
  expect(wrapper.find('SignIn')).toHaveLength(1);
});
