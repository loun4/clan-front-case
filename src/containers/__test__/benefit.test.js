import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import mockState from './mock-state';
import ConnectedBenefit, { Benefit } from '../benefit';

const mockStore = configureStore();

const routerProps = {
  match: {
    params: {
      slug: 'subvention-des-activites',
    },
  },
  history: {
    push: jest.fn(),
  },
};

let wrapper;
let mockGetEntityData;
beforeEach(() => {
  mockGetEntityData = jest.fn(() => Promise.resolve({}));
  wrapper = shallow(
    <Benefit
      getEntityData={mockGetEntityData}
      benefit={mockState.getIn(['entities', 'benefits', 'models', 0])}
      {...routerProps}
    />
  );
});

test('Render benefits', async () => {
  const component = renderer.create(
    <Router>
      <ConnectedBenefit
        store={mockStore(mockState)}
        getEntityData={mockGetEntityData}
        {...routerProps}
      />
    </Router>
  );

  const instance = component.getInstance();
  await instance.componentDidMount();
  expect(component.toJSON()).toMatchSnapshot();
});

test('Call history.push on close', () => {
  wrapper.find('#close').simulate('click', {});
  expect(routerProps.history.push.mock.calls[0][0]).toEqual('/');
});
