import { Map, fromJS } from 'immutable';

export default Map({
  session: Map({
    user: Map({
      first_name: 'foo',
      last_name: 'bar',
    }),
  }),
  entities: Map({
    benefits: Map({
      isFetching: false,
      isRequested: true,
      models: fromJS([
        {
          id: 1,
          name: 'Subvention des activités sportives',
          description:
            'Subvention de toutes les activités sportives des salariés sur présentation de factures payées',
          slug: 'subvention-des-activites',
          createdAt: '2019-01-14T12:49:44.388Z',
          updatedAt: '2019-01-14T12:49:45.774Z',
          currentPeriod: {
            id: 1,
            state: 'current',
            createdAt: '2019-01-14T12:49:44.715Z',
            updatedAt: '2019-01-14T12:49:48.651Z',
            openingDateFormatted: '01/01/2019',
            closingDateFormatted: '31/12/2019',
            remainingDaysBeforeEnd: 204,
            totalAllowedAmount: 350,
            numberOfParticipants: 4,
          },
        },
        {
          id: 2,
          name: 'Subvention des activités culturelles',
          description:
            'Subvention de toutes les activités culturelles des salariés sur présentation de factures payées',
          slug: 'subvention-des-activites2',
          createdAt: '2019-01-14T12:49:44.435Z',
          updatedAt: '2019-01-14T12:49:45.829Z',
          currentPeriod: {
            id: 2,
            state: 'current',
            createdAt: '2019-01-14T12:49:44.793Z',
            updatedAt: '2019-01-14T12:49:48.755Z',
            openingDateFormatted: '01/01/2019',
            closingDateFormatted: '31/12/2019',
            remainingDaysBeforeEnd: 204,
            totalAllowedAmount: 1500,
            numberOfParticipants: 2,
          },
        },
      ]),
    }),
  }),
});
