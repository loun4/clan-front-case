import React, { Component } from 'react';
import { connect } from 'react-redux';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Route } from 'react-router-dom';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import numeral from 'numeral';
import Benefit from './benefit';
import Loader from '../components/loader';
import PeriodProgress from '../components/period-progress';
import { getEntityData } from '../actions/entities';
import { mergeProps } from '../helpers';

class Benefits extends Component {
  static propTypes = {
    getEntityData: PropTypes.func.isRequired,
    benefits: ImmutablePropTypes.map.isRequired,
    history: PropTypes.shape().isRequired,
  };

  componentDidMount() {
    this.props.getEntityData({
      entity: 'benefits',
      endpoint: 'employee/damien/benefits',
    });
  }

  renderBenefit = ({
    id,
    slug,
    name,
    description,
    currentPeriod: {
      openingDateFormatted,
      closingDateFormatted,
      remainingDaysBeforeEnd,
      totalAllowedAmount,
      numberOfParticipants,
    },
  }) => (
    <tr
      key={`benefit-${id}`}
      className="is-link"
      onClick={() => this.props.history.push(`/${slug}`)}
    >
      <td>{name}</td>
      <td>{description}</td>
      <td className="is-headerless is-progress-col">
        <PeriodProgress
          openingDateFormatted={openingDateFormatted}
          closingDateFormatted={closingDateFormatted}
          remainingDaysBeforeEnd={remainingDaysBeforeEnd}
        />
      </td>
      <td className="is-numeric">
        {numeral(totalAllowedAmount).format('0,000 $')}
      </td>
      <td className="is-numeric">{numberOfParticipants}</td>
    </tr>
  );

  render() {
    const {
      isFetching,
      isRequested,
      models: benefits,
    } = this.props.benefits.toJS();
    if (!isRequested || isFetching) {
      return <Loader />;
    }

    return (
      <div className="section">
        <div className="card">
          <header className="card-header">
            <p className="card-header-title">Subventions</p>
          </header>
          <div className="table-wrapper">
            <table className="table is-striped has-mobile-cards is-hoverable">
              <thead>
                <tr>
                  <th>
                    <div className="th-wrap">Nom</div>
                  </th>
                  <th>
                    <div className="th-wrap">Description</div>
                  </th>
                  <th>
                    <div className="th-wrap">Etat</div>
                  </th>
                  <th className="is-numeric">
                    <div className="th-wrap">Montant maximum</div>
                  </th>
                  <th className="is-numeric">
                    <div className="th-wrap">Nombre de participants</div>
                  </th>
                </tr>
              </thead>
              <tbody>{benefits.map(this.renderBenefit)}</tbody>
            </table>
          </div>
        </div>
        <Route exact path="/:slug" component={Benefit} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  benefits: state.getIn(['entities', 'benefits']),
});

export default withRouter(
  connect(
    mapStateToProps,
    { getEntityData },
    mergeProps
  )(Benefits)
);
