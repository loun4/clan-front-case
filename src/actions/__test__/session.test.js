import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { authenticate, deauthenticate } from '../session';

const mockStore = configureMockStore([thunk]);
const data = { data: { firstName: 'foo', lastName: 'bar' } };

test('authenticate dispatch API_CLEAR_ERROR/AUTHENTICATION_REQUESTED/AUTHENTICATED', () => {
  const store = mockStore({});
  fetch.resetMocks();
  fetch.mockResponseOnce(JSON.stringify(data));

  return store
    .dispatch(
      authenticate({
        email: 'test@test.com',
        password: 123456,
      })
    )
    .then(() => {
      const storeActions = store.getActions();
      const expectedActions = [
        { type: 'API_CLEAR_ERROR' },
        { type: 'REQUEST_AUTHENTICATION', globalFetching: true },
        { type: 'AUTHENTICATED', user: { firstName: 'foo', lastName: 'bar' } },
      ];

      expect(storeActions).toEqual(expectedActions);
    });
});

test('deauthenticate dispatch API_CLEAR_ERROR/DEAUTHENTICATED', () => {
  const store = mockStore({});
  fetch.resetMocks();
  fetch.mockResponseOnce(JSON.stringify(data));

  store.dispatch(deauthenticate());
  const storeActions = store.getActions();
  const expectedActions = [
    { type: 'API_CLEAR_ERROR' },
    { type: 'DEAUTHENTICATED' },
  ];

  expect(storeActions).toEqual(expectedActions);
});
