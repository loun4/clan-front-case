import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getEntityData } from '../entities';

const mockStore = configureMockStore([thunk]);
const payload = {
  entity: 'test',
  endpoint: 'test',
};

test('getEntityData dispatch ENTITY_DATA_REQUESTED/ENTITY_DATA_SUCCEEDED', () => {
  const store = mockStore({});
  fetch.resetMocks();
  fetch.mockResponseOnce(JSON.stringify({}));

  return store.dispatch(getEntityData(payload)).then(() => {
    const storeActions = store.getActions();
    const expectedActions = [
      { type: 'ENTITY_DATA_REQUESTED', entity: 'test', globalFetching: true },
      { type: 'ENTITY_DATA_SUCCEEDED', entity: 'test', data: {} },
    ];

    expect(storeActions).toEqual(expectedActions);
  });
});

test('getEntityData call API with correct URL', () => {
  const store = mockStore({});
  fetch.resetMocks();
  fetch.mockResponseOnce(JSON.stringify({}));

  store.dispatch(getEntityData(payload));

  expect(fetch).toHaveBeenCalledWith('/api/v1/test', {
    headers: { 'Content-Type': 'application/json' },
    method: 'GET',
  });
});

test('getEntityData dispatch DEAUTHENTICATED if unauthorized', () => {
  const store = mockStore({});
  fetch.resetMocks();
  fetch.mockResponseOnce(
    JSON.stringify({
      errors: [],
      success: false,
    }),
    { status: 401, ok: false }
  );

  return store.dispatch(getEntityData(payload)).then(() => {
    const storeActions = store.getActions();
    const expectedActions = [
      { type: 'ENTITY_DATA_REQUESTED', entity: 'test', globalFetching: true },
      { type: 'API_CLEAR_ERROR' },
      { type: 'DEAUTHENTICATED' },
    ];

    expect(storeActions).toEqual(expectedActions);
  });
});
