export const API_GET_ERROR = 'API_GET_ERROR';
export const API_CLEAR_ERROR = 'API_CLEAR_ERROR';

export const apiGetError = (entity, error = {}) => ({
  type: API_GET_ERROR,
  entity,
  error,
});

export const apiClearError = () => ({
  type: API_CLEAR_ERROR,
});
