import Client from '../services/api-client';
import { apiGetError } from './api-errors';
import { deauthenticate } from './session';

export const ENTITY_DATA_REQUESTED = 'ENTITY_DATA_REQUESTED';
export const ENTITY_DATA_SUCCEEDED = 'ENTITY_DATA_SUCCEEDED';

const entityDataRequested = (entity, globalFetching) => ({
  type: ENTITY_DATA_REQUESTED,
  entity,
  globalFetching,
});

const entityDataSucceeded = (entity, data) => ({
  type: ENTITY_DATA_SUCCEEDED,
  entity,
  data,
});

export const getEntityData = ({
  entity,
  endpoint,
  globalFetching = true,
}) => dispatch => {
  dispatch(entityDataRequested(entity, globalFetching));

  return Client.request({ endpoint: `/api/v1/${endpoint}` })
    .then(data => dispatch(entityDataSucceeded(entity, data)))
    .catch(e => dispatch(e.status === 401 ? deauthenticate() : apiGetError(e)));
};
