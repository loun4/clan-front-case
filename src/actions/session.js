import { apiClearError } from './api-errors';
import Client from '../services/api-client';

export const AUTHENTICATION_REQUESTED = 'REQUEST_AUTHENTICATION';
export const AUTHENTICATION_FAILED = 'AUTHENTICATION_FAILED';
export const AUTHENTICATED = 'AUTHENTICATED';
export const DEAUTHENTICATED = 'DEAUTHENTICATED';

const requestAuthentication = globalFetching => ({
  type: AUTHENTICATION_REQUESTED,
  globalFetching,
});

const failedAuthentication = () => ({
  type: AUTHENTICATION_FAILED,
});

const authenticated = user => ({
  type: AUTHENTICATED,
  user,
});

const deauthenticated = () => ({
  type: DEAUTHENTICATED,
});

export const authenticate = ({
  email,
  password,
  globalFetching = true,
}) => dispatch => {
  dispatch(apiClearError());
  dispatch(requestAuthentication(globalFetching));

  return Client.authenticate({ email, password })
    .then(({ data: user }) => {
      dispatch(authenticated(user));
    })
    .catch(error => {
      dispatch(failedAuthentication());
      throw error;
    });
};

export const deauthenticate = () => dispatch => {
  Client.reset();
  dispatch(apiClearError());
  dispatch(deauthenticated());
};
