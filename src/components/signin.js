import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

export default class SignIn extends Component {
  static propTypes = {
    authenticate: PropTypes.func.isRequired,
  };

  state = {
    email: '',
    password: '',
    isValidEmail: true,
    isValidPassword: true,
    isFetching: false,
    isFailed: false,
  };

  handleInputChange = e =>
    this.setState({
      [e.target.name]: e.target.value,
    });

  handleSubmit = e => {
    e.preventDefault();

    const isValid = this.validate();
    if (!isValid) {
      return false;
    }

    const { email, password } = this.state;

    this.setState({ isFetching: true, isFailed: false });

    return this.props
      .authenticate({ email, password, globalFetching: false })
      .catch(() => this.setState({ isFetching: false, isFailed: true }));
  };

  validate() {
    const { email, password } = this.state;
    const isValidEmail = email.length > 0;
    const isValidPassword = password.length > 0;

    this.setState({
      isValidEmail,
      isValidPassword,
    });

    return isValidEmail && isValidPassword;
  }

  render() {
    const {
      email,
      password,
      isValidEmail,
      isValidPassword,
      isFetching,
      isFailed,
    } = this.state;

    return (
      <div className="hero-body">
        <div className="container has-text-centered">
          <div className="column is-6 is-offset-3">
            <img src="/imgs/logo.png" alt="Clan" className="signin-logo" />
            <div className="box">
              <form onSubmit={this.handleSubmit}>
                <div className="field">
                  <div className="control">
                    <input
                      className={cx('input is-large', {
                        'is-danger': !isValidEmail,
                      })}
                      name="email"
                      type="email"
                      placeholder="Email"
                      value={email}
                      onChange={this.handleInputChange}
                    />
                    {!isValidEmail && (
                      <p className="help is-danger">Email invalide</p>
                    )}
                  </div>
                </div>

                <div className="field">
                  <div className="control">
                    <input
                      className={cx('input is-large', {
                        'is-danger': !isValidPassword,
                      })}
                      type="password"
                      name="password"
                      placeholder="Mot de passe"
                      autoComplete="new-password"
                      value={password}
                      onChange={this.handleInputChange}
                    />
                    {!isValidPassword && (
                      <p className="help is-danger">Mot de passe invalide</p>
                    )}
                  </div>
                </div>

                <button
                  type="submit"
                  className={cx(
                    'button is-block signin-button is-large is-fullwidth',
                    {
                      'is-loading ': isFetching,
                    }
                  )}
                >
                  Connexion
                </button>

                {isFailed && (
                  <p id="error" className="help is-danger spacer">
                    Connexion refusée, veuillez vérifier vos identifiants
                  </p>
                )}
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
