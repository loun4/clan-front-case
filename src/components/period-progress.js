import React from 'react';
import PropTypes from 'prop-types';
import { localStringToDate } from '../helpers';

const PeriodProgress = ({
  openingDateFormatted,
  closingDateFormatted,
  remainingDaysBeforeEnd,
}) => {
  const openingDate = localStringToDate(openingDateFormatted);
  const closingDate = localStringToDate(closingDateFormatted);
  const totalTime = Math.abs(closingDate.getTime() - openingDate.getTime());
  const totalDays = Math.ceil(totalTime / (1000 * 60 * 60 * 24));
  const hasStarted = new Date() >= openingDate;
  const elapsedDays = hasStarted ? totalDays - remainingDaysBeforeEnd : 5;

  return (
    <React.Fragment>
      <progress
        max={totalDays}
        value={elapsedDays}
        className="progress is-small"
      >
        {elapsedDays}
      </progress>
      <small className="has-text-grey is-abbr-like">
        {hasStarted
          ? `${remainingDaysBeforeEnd} jours restants | ${closingDateFormatted}`
          : `Commence le ${openingDateFormatted}`}
      </small>
    </React.Fragment>
  );
};

PeriodProgress.propTypes = {
  openingDateFormatted: PropTypes.string.isRequired,
  closingDateFormatted: PropTypes.string.isRequired,
  remainingDaysBeforeEnd: PropTypes.number,
};

PeriodProgress.defaultProps = {
  remainingDaysBeforeEnd: null,
};

export default PeriodProgress;
