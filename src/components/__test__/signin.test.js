import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import SignIn from '../signin';

let mockAuthenticate;
let wrapper;
const preventDefault = { preventDefault() {} };

const makeWrapper = mockFn => shallow(<SignIn authenticate={mockFn} />);

const submitForm = w => {
  w.find('input[type="email"]').simulate('change', {
    target: { name: 'email', value: 'test@test.com' },
  });

  w.find('input[type="password"]').simulate('change', {
    target: { name: 'password', value: '123456' },
  });

  w.find('form').simulate('submit', preventDefault);
};

beforeEach(() => {
  mockAuthenticate = jest.fn(() => Promise.resolve({}));
  wrapper = makeWrapper(mockAuthenticate);
});

test('Render SignIn', () => {
  const tree = renderer
    .create(<SignIn authenticate={mockAuthenticate} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test('Render with email/password', () => {
  expect(wrapper.find('input[type="email"]')).toHaveLength(1);
  expect(wrapper.find('input[type="password"]')).toHaveLength(1);
});

describe('Empty email/password', () => {
  test('Update state', () => {
    wrapper.find('form').simulate('submit', preventDefault);
    expect(wrapper.state('isValidEmail')).toBe(false);
    expect(wrapper.state('isValidPassword')).toBe(false);
  });

  test('Display errors', () => {
    wrapper.find('form').simulate('submit', preventDefault);

    expect(wrapper.find('input[type="email"]').hasClass('is-danger')).toBe(
      true
    );
    expect(wrapper.find('input[type="password"]').hasClass('is-danger')).toBe(
      true
    );
    expect(wrapper.find('.help.is-danger')).toHaveLength(2);
  });
});

test('Call authenticate with email/password/globalFetching as arguments', () => {
  submitForm(wrapper);

  expect(mockAuthenticate.mock.calls[0][0]).toEqual({
    email: 'test@test.com',
    password: '123456',
    globalFetching: false,
  });
});

test('Display loader indicator while fetching', () => {
  submitForm(wrapper);
  expect(wrapper.state('isFetching')).toBe(true);
  expect(wrapper.find('button[type="submit"]').hasClass('is-loading')).toBe(
    true
  );
});

describe('Authentication fails', () => {
  test('Update state', async () => {
    mockAuthenticate = jest.fn(() => Promise.reject('error'));
    wrapper = makeWrapper(mockAuthenticate);
    await submitForm(wrapper);
    wrapper.update();

    expect(wrapper.state('isFetching')).toBe(false);
    expect(wrapper.state('isFailed')).toBe(true);
  });

  test('Display error', async () => {
    mockAuthenticate = jest.fn(() => Promise.reject('error'));
    wrapper = makeWrapper(mockAuthenticate);
    await submitForm(wrapper);
    wrapper.update();

    expect(wrapper.find('#error')).toHaveLength(1);
  });
});
