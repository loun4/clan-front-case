import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import { Map } from 'immutable';
import Header from '../header';

const user = Map({
  first_name: 'foo',
  last_name: 'bar',
});

let wrapper;
let mockDeauthenticate;

beforeEach(() => {
  mockDeauthenticate = jest.fn();
  wrapper = shallow(<Header deauthenticate={mockDeauthenticate} user={user} />);
});

test('Render Header', () => {
  const tree = renderer
    .create(<Header deauthenticate={mockDeauthenticate} user={user} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test('Render user full name', () => {
  expect(wrapper.find('button').text()).toContain('foo bar');
});

test('Call deauthenticate', () => {
  wrapper.find('button').simulate('click');
  expect(mockDeauthenticate).toHaveBeenCalled();
});
