import React from 'react';
import renderer from 'react-test-renderer';
import PeriodProgress from '../period-progress';

test('Render PeriodProgress', () => {
  const tree = renderer
    .create(
      <PeriodProgress
        openingDateFormatted="01/01/2019"
        closingDateFormatted="30/06/2019"
        remainingDaysBeforeEnd={10}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
