/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';

const Header = ({ user, deauthenticate }) => {
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <a className="navbar-item" href="https://bulma.io">
          <img src="/imgs/logo.png" alt="Clan" />
        </a>
      </div>
      <div id="navbarBasicExample" className="navbar-menu">
        <div className="navbar-end">
          <div className="navbar-item">
            <div className="buttons">
              <button
                type="button"
                className="button is-primary"
                onClick={deauthenticate}
              >
                <strong>
                  {user.get('first_name')} {user.get('last_name')}
                  {' - '}
                  se déconnecter
                </strong>
              </button>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};

Header.propTypes = {
  user: ImmutablePropTypes.map.isRequired,
  deauthenticate: PropTypes.func.isRequired,
};

export default Header;
