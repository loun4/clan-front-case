import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Loader = ({ className }) => (
  <div className={classNames('app-loader', className)}>
    <img src="/imgs/loader.svg" alt="" width="60" height="60" />
  </div>
);

Loader.propTypes = {
  className: PropTypes.string,
};

Loader.defaultProps = {
  className: null,
};

export default Loader;
