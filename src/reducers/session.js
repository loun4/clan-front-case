import { Map } from 'immutable';
import Client from '../services/api-client';

import {
  AUTHENTICATION_REQUESTED,
  AUTHENTICATION_FAILED,
  AUTHENTICATED,
  DEAUTHENTICATED,
} from '../actions/session';

export const initialState = Map({
  credentials: Client.getCredentials(),
  isFetching: false,
  isAuthenticated: false,
  user: Map({}),
});

const session = (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATION_REQUESTED:
      return state
        .set('credentials', null)
        .set('isFetching', action.globalFetching)
        .set('isAuthenticated', false);

    case AUTHENTICATED:
      return state
        .set('credentials', null)
        .set('isFetching', false)
        .set('isAuthenticated', true)
        .set('user', Map(action.user));

    case AUTHENTICATION_FAILED:
    case DEAUTHENTICATED:
      return initialState.set('credentials', null);
    default:
      return state;
  }
};

export default session;
