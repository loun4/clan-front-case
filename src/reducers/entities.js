/* eslint-disable no-param-reassign */
import { Map, List, fromJS } from 'immutable';
import {
  ENTITY_DATA_REQUESTED,
  ENTITY_DATA_SUCCEEDED,
} from '../actions/entities';
import { DEAUTHENTICATED } from '../actions/session';

export const initialEntityState = Map({
  isFetching: false,
  isRequested: false,
  models: List(),
});

const initialState = Map({
  benefits: initialEntityState,
});

export default (
  state = initialState,
  { type, entity, data, globalFetching }
) => {
  switch (type) {
    case ENTITY_DATA_REQUESTED:
      return state.update(entity, e =>
        e.set('isFetching', globalFetching).set('isRequested', true)
      );

    case ENTITY_DATA_SUCCEEDED:
      return state.update(entity, entityState => {
        entityState = entityState.set('isFetching', false);

        if (Array.isArray(data)) {
          return entityState.set('models', fromJS(data));
        }

        return entityState.update('models', models =>
          models.map(model =>
            model.get('slug') === data.slug ? fromJS(data) : model
          )
        );
      });

    case DEAUTHENTICATED:
      return initialState;

    default:
      return state;
  }
};
