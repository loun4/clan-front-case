import { Map } from 'immutable';
import session, { initialState } from '../session';

test('Handle AUTHENTICATION_REQUESTED', () => {
  const action = { type: 'AUTHENTICATION_REQUESTED' };
  const expectedState = Map({
    credentials: null,
    isFetching: false,
    isAuthenticated: false,
    user: Map({}),
  });

  expect(session(initialState, action)).toEqual(expectedState);
});

test('Handle AUTHENTICATED', () => {
  const action = { type: 'AUTHENTICATED', user: { first_name: 'foo' } };
  const expectedState = Map({
    credentials: null,
    isFetching: false,
    isAuthenticated: true,
    user: Map({ first_name: 'foo' }),
  });

  expect(session(initialState, action)).toEqual(expectedState);
});

test('Handle DEAUTHENTICATED', () => {
  const action = { type: 'DEAUTHENTICATED' };
  expect(session(initialState, action)).toEqual(initialState);
});
