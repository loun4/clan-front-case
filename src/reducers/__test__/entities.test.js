import { Map, List } from 'immutable';
import entities, { initialEntityState } from '../entities';

const initialState = Map({
  test: initialEntityState,
});

test('Handle ENTITY_DATA_REQUESTED', () => {
  const action = {
    type: 'ENTITY_DATA_REQUESTED',
    entity: 'test',
    globalFetching: false,
  };

  const expectedState = Map({
    test: Map({
      isFetching: false,
      isRequested: true,
      models: List(),
    }),
  });

  expect(entities(initialState, action)).toEqual(expectedState);
});

test('Handle ENTITY_DATA_SUCCEEDED', () => {
  const action = {
    type: 'ENTITY_DATA_SUCCEEDED',
    entity: 'test',
    data: [{ name: 'test' }],
    globalFetching: false,
  };

  const state = Map({
    test: Map({
      isFetching: false,
      isRequested: true,
      models: List(),
    }),
  });

  const expectedState = Map({
    test: Map({
      isFetching: false,
      isRequested: true,
      models: List.of(Map({ name: 'test' })),
    }),
  });

  expect(entities(state, action)).toEqual(expectedState);
});
