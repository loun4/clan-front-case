// eslint-disable-next-line
import storage from 'store2';
import Client from '../api-client';

const credentials = {
  email: 'foo@bar.com',
  password: 123456,
};

const fetchHeaders = {
  'access-token': 'xxx',
  'cache-control': 'max-age=0, private, must-revalidate',
  'content-type': 'application/json; charset=utf-8',
  'token-type': 'Bearer',
  uid: 'foo@bar.com',
  client: 'xxxx',
  expiry: 'xxx',
};

const resHeaders = {
  'access-token': 'xxx',
  'token-type': 'Bearer',
  uid: 'foo@bar.com',
  client: 'xxxx',
  expiry: 'xxx',
};

test('Throw error if invalid credentials', () => {
  const authenticate = () => {
    Client.authenticate({});
  };

  expect(authenticate).toThrow();
});

test('Authenticate with correct URL', () => {
  fetch.mockResponse(JSON.stringify({}), { status: 200 });
  Client.authenticate(credentials);

  expect(fetch).toHaveBeenCalledWith(
    '/auth/sign_in?email=foo@bar.com&password=123456',
    { method: 'POST', headers: { 'Content-Type': 'application/json' } }
  );
});

test('Pick and save authentication headers', async () => {
  fetch.mockResponse(JSON.stringify({}), {
    status: 200,
    headers: fetchHeaders,
  });

  await Client.authenticate(credentials);
  expect(Client.authHeaders).toEqual(resHeaders);
  expect(storage.get('authHeaders')).toEqual(resHeaders);
});

test('Reject with status code & text', async () => {
  fetch.mockResponseOnce(
    JSON.stringify({
      errors: [],
      success: false,
    }),
    { status: 401, ok: false }
  );

  try {
    await Client.authenticate(credentials);
  } catch (error) {
    expect(error).toEqual({
      status: 401,
      statusText: 'Unauthorized',
    });
  }
});

test('Resolve with JSON', async () => {
  const data = { data: { firstName: 'foo', lastName: 'bar' } };
  fetch.mockResponseOnce(JSON.stringify(data));

  const res = await Client.request({ url: 'test' });
  expect(res).toEqual(data);
});

test('Delete authentication headers', async () => {
  fetch.mockResponse(JSON.stringify({}), {
    status: 200,
    headers: fetchHeaders,
  });

  await Client.authenticate(credentials);
  Client.reset();

  expect(Client.authHeaders).toBeNull();
  expect(storage.get('authHeaders')).toBeNull();
});
