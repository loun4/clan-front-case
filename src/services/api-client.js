import 'cross-fetch';
import storage from 'store2';

const REST_URL =
  process.env.NODE_ENV === 'test' ? '' : process.env.REACT_APP_REST_URL;

const AUTH_HEADER_FIELDS = [
  'access-token',
  'token-type',
  'client',
  'expiry',
  'uid',
];

const validateCredentials = credentials => {
  if (!credentials.email || !credentials.password) {
    throw new Error(
      'credentials should include both email and password fields'
    );
  }
};

class Client {
  authHeaders = storage.get('authHeaders') || null;

  authenticate(credentials) {
    validateCredentials(credentials);

    const { email, password } = credentials;

    return this.request({
      endpoint: `/auth/sign_in?email=${email}&password=${password}`,
      method: 'POST',
      auth: false,
      newHeaders: true,
    })
      .then(data => {
        this.saveCredentials(credentials);
        return data;
      })
      .catch(error => {
        this.reset();
        throw error;
      });
  }

  request({ endpoint, method = 'GET', auth = true, newHeaders = false }) {
    return new Promise((resolve, reject) => {
      return fetch(`${REST_URL}${endpoint}`, {
        method,
        headers: {
          'Content-Type': 'application/json',
          ...(auth && this.getAuthHeaders()),
        },
      })
        .then(res => {
          if (!res.ok) {
            return res.json().then(() =>
              reject({
                status: res.status,
                statusText: res.statusText,
              })
            );
          }

          if (newHeaders) {
            this.saveAuthHeaders(res.headers);
          }
          return resolve(res.json(), res.headers);
        })
        .catch(() => reject({ status: null, statusText: 'unhandledIssue' }));
    });
  }

  reset() {
    this.authHeaders = null;
    storage.remove('credentials');
    storage.remove('authHeaders');
    return this;
  }

  saveAuthHeaders(headers) {
    this.authHeaders = AUTH_HEADER_FIELDS.reduce((memo, field) => {
      if (!headers.get(field)) return memo;
      return { ...memo, [field]: headers.get(field) };
    }, {});

    storage.set('authHeaders', this.authHeaders);
    return this;
  }

  saveCredentials = credentials => {
    storage.set('credentials', credentials);
  };

  getAuthHeaders() {
    return this.authHeaders;
  }

  // eslint-disable-next-line class-methods-use-this
  getCredentials() {
    return storage.get('credentials');
  }
}

export default new Client();
