import numeral from 'numeral';

// Allow dispatchProps to be overriden
// so  props can be mocked & tested
// eslint-disable-next-line
export const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export const localStringToDate = str => {
  const [day, month, year] = str.split('/');
  return new Date(`${month}/${day}/${year}`);
};

// load a locale
numeral.register('locale', 'fr', {
  delimiters: {
    thousands: ' ',
    decimal: ',',
  },
  abbreviations: {
    thousand: 'k',
    million: 'm',
    billion: 'b',
    trillion: 't',
  },
  ordinal(number) {
    return number === 1 ? 'er' : 'ème';
  },
  currency: {
    symbol: '€',
  },
});

numeral.locale('fr');
